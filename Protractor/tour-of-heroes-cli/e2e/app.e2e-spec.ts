import { TourOfHeroesPage } from './app.po';

/*describe('Tour of heroes Dashboard', () => {
  let page: TourOfHeroesPage;

  beforeEach(() => {
    page = new TourOfHeroesPage();
  });

  it('should display top 4 heroes', () => {
    page.navigateTo();
    expect(page.getTop4Heroes()).toEqual(['Mr. Nice', 'Narco', 'Bombasto', 'Celeritas']);
  });

  it('should navigate to heroes', () => {
    page.navigateToHeroes();
    expect(page.getAllHeroes().count()).toBe(11);
  });
});

describe('Tour of heroes, heroes page', () => {
  let page: TourOfHeroesPage;

  beforeEach(() => {
    page = new TourOfHeroesPage;
    page.navigateToHeroes();
  });

  it('should add a new hero', () => {
    const currentHeroes = page.getAllHeroes().count();
    page.enterNewHeroInInput('My new Hero');
    expect(page.getAllHeroes().count()).toBe(currentHeroes.then(n => n + 1));
  });

});*/


/*
2.4 Su turno

Ahora es su turno de añadir las siguientes pruebas:

Buscar Heroes
Eliminar un heroe
Editar un heroe
Navegar a un heroe desde el dashboard
Navegar a un heroe desde la lista de heroes
Navegar a un heroe desde la busqueda

*/

describe('Mi turno 1', () => {
  let page: TourOfHeroesPage;

  beforeEach(() => {
    page = new TourOfHeroesPage;
    page.navigateTo();
  });

  it('buscar heroes', () => {
    expect(page.searchAnHero('Z')).toEqual("Zero");
  });

  it('editar heroe', () => {
    expect(
      page.editHero('Narco', 'Capo')
        .then(() => page.searchAnHero('Cap'))
    ).toEqual("Capo");
  });

  it('Navegar a un heroe desde el dashboard', () => {
    page.goToHero('Narco');
    expect(page.getHeroTitle()).toEqual('Narco details!');
  });
  it('Navegar a un heroe desde la busqueda', () => {
    page.goToHero('Narco');
    expect(page.getHeroTitle()).toEqual('Narco details!');
  });






});

describe('Mi turno 2', () => {
  let page: TourOfHeroesPage;

  beforeEach(() => {
    page = new TourOfHeroesPage;
    page.navigateToHeroes();
  });

  it('eliminar un heroe', () => {
    expect(page.getHero('Zero').getTagName()).toBe('li');
    page.deleteHero('Zero');
    expect(page.exist('Zero')).toBe(false);
  });

  it('Navegar a un heroe desde la lista de heroes', () => {

    expect(page.getHero('Bombasto').click()
      .then(() => page.viewHeroDetails())
      .then(() => page.getHeroTitle())
    ).toEqual('Bombasto details!');
  })

});

