import { browser, by, element, ElementFinder, Key } from 'protractor';

export class TourOfHeroesPage {
  navigateTo() {
    return browser.get('/');
  }

  getTop4Heroes() {
    return element.all(by.css('.module.hero')).all(by.tagName('h4')).getText();
  }

  navigateToHeroes() {
    element(by.linkText('Heroes')).click();
  }

  getAllHeroes() {
    return element(by.tagName('my-heroes')).all(by.tagName('li'));
  }

  enterNewHeroInInput(newHero: string) {
    element(by.tagName('input')).sendKeys(newHero);
    element(by.buttonText('Add')).click();
  }

  searchAnHero(heroName: string) {
    element(by.id('search-box')).sendKeys(heroName + Key.ENTER);
    return element(by.css('.search-result')).getText();
  }

  getHero(name: string) {
    return element(by.cssContainingText('span', name)).element(by.xpath('..'));
  }
  deleteHero(name: string) {
    this.getHero(name).element(by.tagName('button')).click();
  }
  exist(name: string) {
    let heroes = this.getAllHeroes();
    return heroes
      .map(heroLI => heroLI.all(by.tagName('span')).get(1).getText())
      .then(listaDeNombres => !!listaDeNombres.filter(heroName => heroName === name)[0]);
  }
  editHero(currentName: string, newName: string) {
    this.goToHero(currentName);
    return element(by.css('input')).clear()
      .then(() => element(by.css('input')).sendKeys(newName))
      .then(() => element(by.cssContainingText('button', 'Save')).click());
  }

  goToHero(name: string) {
    element(by.cssContainingText('h4', name)).click();
  }
  getHeroTitle() {
    return element(by.css('h2')).getText();
  }
  sleep(seconds: number) {
    browser.sleep(seconds * 1000)
  }
  viewHeroDetails() {
    element(by.cssContainingText('button', 'View Details')).click();
  }
}
