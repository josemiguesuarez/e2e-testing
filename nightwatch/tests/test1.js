module.exports = { // adapted from: https://git.io/vodU0
    'Los estudiantes login falied': function (browser) {
        browser
            .url('https://losestudiantes.co/')
            .click('.botonCerrar')
            .pause(1000)
            .waitForElementVisible('.botonIngresar', 4000)
            .click('.botonIngresar')
            .pause(1000)
            .setValue('.cajaLogIn input[name="correo"]', 'wrongemail12345@example.com')
            .setValue('.cajaLogIn input[name="password"]', '1234')
            .click('.cajaLogIn .logInButton')
            .waitForElementVisible('.aviso.alert.alert-danger', 4000)
            .assert.containsText('.aviso.alert.alert-danger', 'El correo y la contraseña que ingresaste no figuran')
            .end();
    },
    'Se dirige a la página de un profesor': function (browser) {
        browser
            .url('https://losestudiantes.co/')
            .click('.botonCerrar')
            .useXpath()
            .waitForElementVisible("//*[contains(text(), 'Alfabético')]", 4000)
            .pause(8000)
            .click("//*[contains(text(), 'Alfabético')]")
            .waitForElementVisible("//*[contains(text(), 'Alvaro Andres')]", 4000)
            .pause(1000)
            .click("//*[contains(text(), 'Alvaro Andres')]")
            .waitForElementVisible("//h1[@class='nombreProfesor']", 4000)
            .pause(1000)
            .assert.containsText("//h1[@class='nombreProfesor']", "Alvaro Andres Gomez D")
            .end();
    },
    'Revisa los filtros por materia en la página de un profesor': function (browser) {
        browser
            .url('https://losestudiantes.co/universidad-de-los-andes/ingenieria-de-sistemas/profesores/alvaro-andres-gomez-d%60alleman')
            .useCss()
            .waitForElementVisible('input[name="ISIS1204"]', 4000)
            .pause(1000)
            .click('input[name="ISIS1204"][type="checkbox"]')
            .pause(1000)
            .click('input[name="ISIS1204"][type="checkbox"]')
            .pause(1000)
            .elements('css selector', 'label[class="labelHover"]', function (result) {
                var self = this;
                result.value.forEach(function (el, j, elz) {
                    self.elementIdText(el.ELEMENT, function (text) {

                        //if (text.value.indexOf('Algoritmica Y Progr. Obj. I') === -1) throw new Error("No se hizo el filtro");
                    });
                });
            })
            .end();
    }
};