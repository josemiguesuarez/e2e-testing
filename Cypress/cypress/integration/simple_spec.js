describe('Los estudiantes login', function () {
    var index = function () {
        cy.visit('https://losestudiantes.co');
        cy.contains('Cerrar').click();
    };
    // 0 tutorial
    it('Visits los estudiantes and fails at login', function () {
        index();
        //Lineas nuevas  
        cy.contains('Ingresar').click();
        cy.get('.cajaLogIn').find('input[name="correo"]').click().type("wrongemail12345@example.com");
        cy.get('.cajaLogIn').find('input[name="password"]').click().type("1234");
        cy.get('.cajaLogIn').contains('Ingresar').click();
        cy.contains('El correo y la contraseña que ingresaste no figuran en la base de datos. Intenta de nuevo por favor.');
    });
    // Punto 1 parte 1
    it('Prueba el login correcto ', function () {
        index();

        cy.contains('Ingresar').click();
        cy.get('.cajaLogIn').find('input[name="correo"]').click().type("cupitallerensayo@gmail.com");
        cy.get('.cajaLogIn').find('input[name="password"]').click().type("ContraseñaDeEjemplo");
        cy.get('.cajaLogIn').contains('Ingresar').click();
        cy.get('div[title="cuenta"]').find('#cuenta');

    });
    // Punto 1 parte 2
    it('La creación de una cuenta con login que ya existe ', function () {
        index();

        cy.contains('Ingresar').click();
        cy.get('.cajaSignUp').within(function (cajaSignUp) {
            cy.get('input[name="nombre"]').click().type("Prueba");
            cy.get('input[name="apellido"]').click().type("Prueba");
            cy.get('input[name="correo"]').click().type("cupitallerensayo@gmail.com");
            cy.get('select[name="idDepartamento"]').select('Ingeniería de Sistemas');
            cy.get('input[name="password"]').click().type("ContraseñaDeEjemplo");
            cy.contains('Registrarse').click();
        });
        cy.contains('Ya existe un usuario registrado con el correo');
    });
    // Punto 2
    it('Busca un profesor', function () {
        index();

        cy.get('form[class="buscador"]').find('.Select-arrow').click();

        cy.get('form[class="buscador"]').find('input[role="combobox"]').type("mario lina", {
            delay: 200
        });
        cy.wait(1000);
        cy.get('form[class="buscador"]').contains('Mario Linares Vasquez').click();
        cy.get('.infoProfesor');


    });
    // Punto 3
    it('Se dirige a la página de un profesor', function () {
        index();
        cy.contains("Alfabético").click();
        cy.contains("Alvaro Andres Gomez").click();
        cy.get('.infoProfesor');
    });

    // Punto 4
    it('Revisa los filtros por materia en la página de un profesor', function () {
        cy.visit('https://losestudiantes.co/universidad-de-los-andes/ingenieria-de-sistemas/profesores/alvaro-andres-gomez-d%60alleman');
        cy.get('input[name="ISIS1204"]').check();
        cy.wait(10000);
        cy.get('.post').each(function ($li, index, $lis) {
            cy.wrap($li).find('div[class="sobreCalificacion"]').find('label').contains('Algoritmica Y Progr. Obj. I');
        });
    });
});