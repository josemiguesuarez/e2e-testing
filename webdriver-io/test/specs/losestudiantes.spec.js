var assert = require('assert');
describe('los estudiantes login', function () {
    it('should visit los estudiantes and failed at log in', function () {
        browser.url('https://losestudiantes.co');
        browser.click('button=Cerrar');
        browser.pause(600);
        browser.waitForVisible('button=Ingresar', 5000);
        browser.click('button=Ingresar');

        var cajaLogIn = browser.element('.cajaLogIn');
        var mailInput = cajaLogIn.element('input[name="correo"]');

        mailInput.click();
        mailInput.keys('wrongemail12345@example.com');

        var passwordInput = cajaLogIn.element('input[name="password"]');

        passwordInput.click();
        passwordInput.keys('1234');

        cajaLogIn.element('button=Ingresar').click();
        browser.waitForVisible('.aviso.alert.alert-danger', 5000);

        var alertText = browser.element('.aviso.alert.alert-danger').getText();
        expect(alertText).toBe('Upss! El correo y la contraseña que ingresaste no figuran en la base de datos. Intenta de nuevo por favor.');
    });


    it('La creación de una cuenta con login que ya existe ', function () {
        browser.url('https://losestudiantes.co');
        browser.waitForVisible('button=Ingresar', 5000);
        browser.click('button=Ingresar');

        var cajaSignUp = browser.element('.cajaSignUp');

        write(cajaSignUp.element('input[name="nombre"]'), "Prueba");
        write(cajaSignUp.element('input[name="apellido"]'), "Prueba");
        write(cajaSignUp.element('input[name="correo"]'), "cupitallerensayo@gmail.com");
        write(cajaSignUp.element('input[name="password"]'), "ContraseñaDeEjemplo");

        cajaSignUp.element('select[name="idDepartamento"]').selectByVisibleText('Ingeniería de Sistemas');
        cajaSignUp.element('button=Registrarse').click();
        browser.waitForVisible('.sweet-alert', 5000);
        var alertText = browser.element('.sweet-alert').element('h2').getText();
        expect(alertText).toBe('Ocurrió un error activando tu cuenta');
    });

    function write(element, text) {
        element.click();
        element.keys(text);
    }
    it('Prueba el login correcto ', function () {
        browser.url('https://losestudiantes.co');
        browser.waitForVisible('button=Ingresar', 5000);
        browser.click('button=Ingresar');

        var cajaLogIn = browser.element('.cajaLogIn');
        var mailInput = cajaLogIn.element('input[name="correo"]');

        mailInput.click();
        mailInput.keys('cupitallerensayo@gmail.com');

        var passwordInput = cajaLogIn.element('input[name="password"]');

        passwordInput.click();
        passwordInput.keys('ContraseñaDeEjemplo');

        cajaLogIn.element('button=Ingresar').click();
        browser.waitForVisible('#cuenta', 5000);
    });

    it('Busca un profesor', function () {
        browser.url('https://losestudiantes.co');
        var buscador = browser.element('form[class="buscador"]');
        buscador.element('.Select-arrow').click();
        buscador.element('input[role="combobox"]').keys('mario lin');

        browser.waitForExist('.Select-option', 5000);

        browser.waitUntil(function () {
            return browser.getText('.Select-option').indexOf('Mario Linares Vasquez') >= 0;
        }, 5000, 'expected text to be different after 5s');

    });


});